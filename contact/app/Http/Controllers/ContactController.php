<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Email;
use App\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;


class ContactController extends Controller
{

    public function contactAdd(Request $request)
    {

        $name = $request->name;
        $phones = $request->phones;
        $emails = $request->emails;
        $birthdate = $request->birthdate;

        $validator = Validator::make(['name'=>$name],['name'=>'required|max:20|unique:contacts,name']);

        if ($validator->fails()){
            return response()->json(['message'=>'name is required and should be maximum 20 characters and should be unique'],422);
        }

        if (!empty($phones)){
            $validator = Validator::make(['phone'=>$phones],['phone'=>'JSON']);

            if ($validator->fails()){
                return response()->json(['message'=>'phone should be json string'],422);
            }
            $phones = json_decode($phones);
        }

        if (!empty($emails)){
            $validator = Validator::make(['email'=>$emails],['email'=>'JSON']);

            if ($validator->fails()){
                return response()->json(['message'=>'email should be json string'],422);
            }
            $emails = json_decode($emails);
        }

        $validator = Validator::make(['birthdate'=>$birthdate],['birthdate'=>"date_format:Y-m-d"]);

        if ($validator->fails()){
            return response()->json(['message'=>'birthdate format should be in yyyy-mm-dd'],422);
        }

        $contact = new Contact;

        $contact->name = $name;
        $contact->birthdate = $birthdate;

        $contact->save();

        $contact_id = $contact->id;

        $now = Carbon::now()->toDateTimeString();

        if (!empty($phones)){

            $phones_db_array = [];

            foreach ($phones as $phone){

                $phones_db_array[] = [
                    'contact_id'=>$contact_id,
                    'phone'=>$phone,
                    'created_at'=>$now,
                    'updated_at'=>$now
                ];

            }

            Phone::insert($phones_db_array);

        }

        if (!empty($emails)){

            $emails_db_array = [];

            foreach ($emails as $email){

                $emails_db_array[] = [
                    'contact_id'=>$contact_id,
                    'email'=>$email,
                    'created_at'=>$now,
                    'updated_at'=>$now
                ];

            }

            Email::insert($emails_db_array);

        }


        return response()->json(['message'=>'contact was added'],200);

    }

    public function contactShow(Request $request)
    {

        $name = $request->name;

        $validator = Validator::make(['name'=>$name],['name'=>'required|max:20']);

        if ($validator->fails()){
            return response()->json(['message'=>'name is required and should be maximum 20 characters'],422);
        }

        $contacts = Contact::with(['phones','emails'])
            ->where('name','like',"%$name%")->get();

        return response()->json(['contacts'=>$contacts],200);

    }

    public function contactUpdate(Request $request)
    {

        $contact_id = $request->id;
        $name = $request->name;
        $phones = $request->phones;
        $emails = $request->emails;
        $birthdate = $request->birthdate;

        $validator = Validator::make(['contact_id'=>$contact_id],['contact_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'id is required and should be integer'],422);
        }

        if (!empty($phones)){
            $validator = Validator::make(['phone'=>$phones],['phone'=>'JSON']);

            if ($validator->fails()){
                return response()->json(['message'=>'phone should be json string'],422);
            }
            $phones = json_decode($phones);
        }

        if (!empty($emails)){
            $validator = Validator::make(['email'=>$emails],['email'=>'JSON']);

            if ($validator->fails()){
                return response()->json(['message'=>'email should be json string'],422);
            }
            $emails = json_decode($emails);
        }

        $validator = Validator::make(['birthdate'=>$birthdate],['birthdate'=>"date_format:Y-m-d"]);

        if ($validator->fails()){
            return response()->json(['message'=>'birthdate format should be in yyyy-mm-dd'],422);
        }

        $contact = Contact::find($contact_id);

        if (!empty($contact)){

            if ($name != $contact->name){
                $validator = Validator::make(['name'=>$name],['name'=>'required|max:20|unique:contacts,name']);

                if ($validator->fails()){
                    return response()->json(['message'=>'name is required and should be maximum 20 characters and should be unique'],422);
                }

            }

            $contact->name = $name;
            $contact->birthdate = $birthdate;

            $contact->save();

            $now = Carbon::now()->toDateTimeString();

            Phone::where('contact_id',$contact_id)->delete();

            Email::where('contact_id',$contact_id)->delete();

            if (!empty($phones)){

                $phones_db_array = [];

                foreach ($phones as $phone){

                    $phones_db_array[] = [
                        'contact_id'=>$contact_id,
                        'phone'=>$phone,
                        'created_at'=>$now,
                        'updated_at'=>$now
                    ];

                }

                Phone::insert($phones_db_array);

            }

            if (!empty($emails)){

                $emails_db_array = [];

                foreach ($emails as $email){

                    $emails_db_array[] = [
                        'contact_id'=>$contact_id,
                        'email'=>$email,
                        'created_at'=>$now,
                        'updated_at'=>$now
                    ];

                }

                Email::insert($emails_db_array);

            }


            return response()->json(['message'=>'contact was updated'],200);


        } else {

            return response()->json(['message'=>'contact was not found'],422);

        }


    }

    public function contactDelete(Request $request)
    {

        $contact_id = $request->id;

        $contact = Contact::find($contact_id);

        if (!empty($contact)){

            Contact::where('id',$contact_id)->delete();

            Phone::where('contact_id',$contact_id)->delete();

            Email::where('contact_id',$contact_id)->delete();

            return response()->json(['message'=>'contact was deleted'],200);


        } else {

            return response()->json(['message'=>'contact was not found'],422);

        }

    }

}
