<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/contactadd','ContactController@contactAdd');
Route::get('/contactshow','ContactController@contactShow');
Route::post('/contactupdate','ContactController@contactUpdate');
Route::post('/contactdelete','ContactController@contactDelete');